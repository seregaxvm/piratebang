#!/usr/bin/python3

"""PirateBang

Usage:
  piratebang.py [--port=<port>] [--baud=<baud>]
  piratebang.py (-h | --help)
  piratebang.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.

"""

import logging
from sys import exit
from docopt import docopt
from typing import Dict
import bpspi as spi
import bpbitbang as bb
from time import sleep

if __name__ == '__main__':
    arguments = docopt(__doc__, version='PirateBang 18.7')
    port = "/dev/ttyACM0" if arguments["--port"] is None \
           else arguments["--port"]
    baud = 9600 if arguments["--baud"] is None or not \
        arguments["--baud"].is_numeric() else arguments["--baud"]
    with spi.BPSPI() as ser:
        ser.configure(port=port, baud="baud")
        if not ser.connect():
            exit(1)
        ser.bitbang()
        ser.spi()
        ser.spi_per_config(spi.POWER | spi.CS["HIGH"])
        ser.spi_io_config(spi.OUT_TYPE["3V3"] |
                          spi.EDGE["A2I"] |
                          spi.IDLE["LOW"])
        ser.spi_speed(spi.SPEED["30KHZ"])
        print(ser.spi_bulk_bytes([1 for _ in range(0, 30)]))
        ser.spi_per_config(0)
        ser.bitbang()
        ser.wait_recieve(1)
        print(ser.smps_start(3000))
        # ser.reset()
