from bpserial import BPSerial
import logging
import logging.config
from typing import List, Optional


CS = 0b_0000_0001
MISO = 0b_0000_0010
CLK = 0b_0000_0100
MOSI = 0b_0000_1000
AUX = 0b_0001_0000
PULLUP = 0b_0010_0000
POWER = 0b_0100_0000
PRESCALER = {1: 0,
             8: 1,
             64: 2,
             256: 3}


class BPBB(BPSerial):
    """BPBB class for BusPirate raw bitbanging"""

    TIME_TEST = 0             # time to wait for test to complete
    _BBMODE = 0x0            # code to enter bitbang mode (repeat 20 times)
    _BBMODE_RES = b"BBIO1"   # return string for bitbang mode
    _SPIMODE = 0x1           # code to enter spi mode
    _SPIMODE_RES = b"SPI1"   # return string for spi mode
    _I2CMODE = 0x2           # code to enter i2c mode
    _I2CMODE_RES = b"I2C1"   # return string for i2c mode
    _UARTMODE = 0x3          # code to enter uart mode
    _UARTMODE_RES = b"ART1"  # return string for uart mode
    _1WMODE = 0x4            # code to enter 1w mode
    _1WMODE_RES = b"1W01"    # return string for 1w mode
    _RAWMODE = 0x5           # code to enter raw wire mode
    _RAWMODE_RES = b"RAW1"   # return string for raw wire mode
    __RESET = 0x0f            # reset BusPirate
    __RESET_RES = b"\x01"     # return string for successful BusPirate reset
    __STEST = 0b_0001_0000    # perform short test
    __LTEST = 0b_0001_0001
    """ perform long test (requires jumpers between +5 and Vpu,
 +3.3 and ADC)"""
    __TEST_INT = 0xff         # interrupt tests
    __TEST_RES = b"\x01"
    """ return string for successful return from test mode"""
    __TEST_DELAY = 1          # seconds to wait for tests to complete
    __PWM_ON = 0b_0001_0010   # enable pwm
    __PWM_OFF = 0b_0001_0011  # disable pwm
    __PWM_RES = b"\x01"       # return string for successful pwm manipulation
    __FOSC = 32e6             # Oscillator frequency
    __VPROBE = 0b_0001_0100   # Measure ADC voltage
    __FPROBE = 0b_0001_0110   # Measure frequency on AUX pin
    __DDR = 0b_0100_0000
    """ Configure data direction for AUX|MOSI|CLK|MISO|CS"""
    __LEVEL = 0b_1000_0000
    """ Configure output level (1/0) for POWER|PULLUP|AUX|MOSI|CLK|MISO|CS"""
    __SCOPE = 0b_0001_0101
    """ Contiuous voltage measure. Not implemented"""
    __SCOPE_STOP = 0b_0001_0110  # Stop contiuous voltage measure
    __XSVF = 0b_0001_1000        # XSVF Player

    # common to modes, not in bitbang
    __GET_MODE = 0x1             # Type and version of the mode
    __START_BIT = 0b_0000_0010   # I2C style start bit
    __STOP_BIT = 0b_0000_0011    # I2C style stop bit
    __CS_LOW = 0b_0000_0100      # CS low (0) (respects hiz setting)
    __CS_HIGH = 0b_0000_0101     # CS high (1)
    __READ_BYTE = 0b_0000_0110   # Read byte
    __READ_BIT = 0b_0000_0111    # Read bit
    __PEEK_PIN = 0b_0000_1000    # Peek at input pin
    __CLOCK_TICK = 0b_0000_1001  # Clock tick
    __CLOCK_LOW = 0b_0000_1010   # Clock low
    __CLOCK_HIGH = 0b_0000_1011  # Clock high
    __DATA_LOW = 0b_0000_1100    # Data low
    __DATA_HIGH = 0b_0000_1101   # Data high
    __BULK_BYTES = 0b_0001_0000  # Bulk transfer, send 1-16 bytes (0=1byte!)
    __BULK_CLOCK = 0b_0010_0000  # Bulk clock ticks, send 1-16 ticks
    __BULK_BITS = 0b_0011_0000
    """Bulk bits, send 1-8 bits of the next byte (0=1bit!)"""
    __BULK_READ = 0b_0101_0000   # Bulk read, read 1-16bytes (0=1byte!)
    __PER_CONFIG = 0b_0100_0000
    """Configure peripherals, w=power, x=pullups, y=AUX, z=CS"""
    __IO_CONFIG = 0b_1000_0000   # Config, w=output type, x=3wire, y=lsb, z=n/a
    # TODO check 2wire and lsb options
    __SET_SPEED = 0b_0110_0000  # Set mode speed
    __GET_SPEED = 0b_0111_0000  # Get mode speed
    _MODE_CONFIRM = b"\x01"     # Conformation code

    # BPv4 Specific Instructions
    __SMPS_GET = 0b_1111_0000   # Return SMPS output voltage
    __SMPS_STOP = 0b_1111_0001  # Stop SMPS operation
    __SMPS_START = 0b_1111_0000
    """Start SMPS operation (xxxx and next byte give requested output voltage)
       Lowest possible value is 512 = 0b 0010 0000"""

    def __init__(self) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)
        self._mode: Optional[int] = None  # holds the current mode

    def __enter_mode(self, mode: int, rv: bytes) -> bool:
        """Internal function to enter mode.
        mode: mode mask.
        Output value: true for success, false for failure"""
        ans = self.send_confirm([mode], [rv])
        return ans[0] if ans is not None else False

    def bitbang(self) -> bool:
        """Enter bitbang mode.
        Output value: true for success, false for failure"""
        for _ in range(0, 20):
            if self.__enter_mode(self._BBMODE, self._BBMODE_RES):
                self.logger.info("Entered bitbang mode")
                self._mode = self._BBMODE
                return True
        self.logger.warning("Could not enter bitbang mode")
        return False

    def spi(self) -> bool:
        """Enter spi mode.
        Output value: true for success, false for failure"""
        if self._mode == self._BBMODE:
            if self.__enter_mode(self._SPIMODE, self._SPIMODE_RES):
                self.logger.info("Entered spi mode")
                self._mode = self._SPIMODE
                return True
            self.logger.warning("Could not enter spi mode")
            return False
        self.logger.warning("Function is avaliable only in bitbang mode")
        return False

    def i2c(self) -> bool:
        """Enter i2c mode.
        Output value: true for success, false for failure"""
        if self._mode == self._BBMODE:
            if self.__enter_mode(self._I2CMODE, self._I2CMODE_RES):
                self.logger.info("Entered i2c mode")
                self._mode = self._I2CMODE
                return True
            self.logger.warning("Could not enter i2c mode")
            return False
        self.logger.warning("Function is avaliable only in bitbang mode")
        return False

    def uart(self) -> bool:
        """Enter uart mode.
        Output value: true for success, false for failure"""
        if self._mode == self._BBMODE:
            if self.__enter_mode(self._UARTMODE, self._UARTMODE_RES):
                self.logger.info("Entered uart mode")
                self._mode = self._UARTMODE
                return True
            self.logger.warning("Could not enter uart mode")
            return False
        self.logger.warning("Function is avaliable only in bitbang mode")
        return False

    def onew(self) -> bool:
        """Enter 1w mode.
        Output value: true for success, false for failure"""
        if self._mode == self._BBMODE:
            if self.__enter_mode(self._1WMODE, self._1WMODE_RES):
                self.logger.info("Entered 1w mode")
                self._mode = self._1WMODE
                return True
            self.logger.warning("Could not enter 1w mode")
            return False
        self.logger.warning("Function is avaliable only in bitbang mode")
        return False

    def raw(self) -> bool:
        """Enter raw mode.
        Output value: true for success, false for failure"""
        if self._mode == self._BBMODE:
            if self.__enter_mode(self._RAWMODE, self._RAWMODE_RES):
                self.logger.info("Entered raw mode")
                self._mode = self._RAWMODE
                return True
            self.logger.warning("Could not enter raw mode")
            return False
        self.logger.warning("Function is avaliable only in bitbang mode")
        return False

    def reset(self) -> bool:
        """Reset BusPirate
        Output value: true for success, false for failure"""
        if self._mode == self._BBMODE:
            self.send_bytes([self.__RESET])
            ans = self.recieve_bytes(1)
            if ans == self.__RESET_RES:
                self.logger.info("BusPirate reset")
                self._mode = None
                self.recieve_all()  # flush buffer
                return True
            else:
                self.logger.info("BusPirate could not reset")
                return False
        self.logger.warning("Function is avaliable only in bitbang mode")
        return False

    def __test(self, mode: int) -> Optional[int]:
        """ self test
        mode: test mode
        Output value: number of errors"""
        self.send_bytes([mode])
        ans: Optional[bytes] = self.wait_recieve(self.TIME_TEST)
        if ans is not None:
            ansi: int = int.from_bytes(ans, byteorder="big")
            if ansi == 0:
                self.logger.info("Self test: Ok")
            else:
                self.logger.warning("Self test discovered %d errors", ansi)
            ansb = self.send_confirm([self.__TEST_INT], [self.__TEST_RES])
            if ansb is not None and ansb[0]:
                self.logger.info("Exited self test mode")
            else:
                self.logger.warning("Cannot exit self test mode")
            return ansi
        else:
            self.logger.warning("Cannot perform self test")
            ansb = self.send_confirm([self.__TEST_INT], [self.__TEST_RES])
            if ansb is not None and ansb[0]:
                self.logger.info("Exited test mode")
            else:
                self.logger.warning("Cannot exit self test mode")
            return None

    def stest(self) -> Optional[int]:
        """Short self test
        Output value: number of errors"""
        if self._mode == self._BBMODE:
            self.logger.info("Starting short self test")
            return self.__test(self.__STEST)
        else:
            self.logger.warning("Function is avaliable only in bitbang mode")
            return None

    def ltest(self) -> Optional[int]:
        """Long self test
        Output value: number of errors"""
        if self._mode == self._BBMODE:
            self.logger.info("Starting long self test")
            return self.__test(self.__LTEST)
        else:
            self.logger.warning("Function is avaliable only in bitbang mode")
            return None

    def pwm_on(self, prescaler: int, period: float, duty: float) -> bool:
        """Enable PWM.
        prescaler: prescaler constant
        period: pwm period in seconds
        duty: duty cycle. Avaliable values from 0 to 1
        Output value: true for success, false for failure

        Configure and enable pulse-width modulation output in the AUX pin.
        Requires a 5 byte configuration sequence.
        Responds 0x01 after a complete sequence is received.
        The PWM remains active after leaving binary bitbang mode!
        Equations to calculate the PWM frequency and period are in the PIC24F
        output compare manual. Bit 0 and 1 of the first configuration byte
        set the prescaler value. The Next two bytes set the duty cycle
        register, high 8bits first. The final two bytes set the period
        register, high 8bits first."""
        if self._mode == self._BBMODE:
            # http://codepad.org/qtYpZmIF
            Tcy = 2.0 / self.__FOSC
            PRy = period / (Tcy * [i for i, j in PRESCALER.items()
                                   if j == prescaler][0]) - 1
            OCR = PRy * duty
            self.send_bytes([self.__PWM_ON,
                             prescaler,
                             ((int(OCR) >> 8) & 0xFF),
                             (int(OCR) & 0xFF),
                             ((int(PRy) >> 8) & 0xFF),
                             (int(PRy) & 0xFF)])
            ans = self.wait_recieve(1)
            if ans == self.__PWM_RES:
                self.logger.info("PWM on")
                return True
            else:
                self.logger.info("PWM setup failed")
                return False
        else:
            self.logger.warning("Function is avaliable only in bitbang mode")
            return False

    def pwm_off(self) -> bool:
        """Disable PWM.
        Output value: true for success, false for failure"""
        if self._mode == self._BBMODE:
            ans = self.send_confirm([self.__PWM_OFF], [self.__PWM_RES])
            if ans is not None and ans[0]:
                self.logger.info("PWM off")
                return True
            else:
                self.logger.info("Cannot disable PWM")
                return False
        else:
            self.logger.warning("Function is avaliable only in bitbang mode")
            return False

    def voltage_raw(self) -> Optional[bytes]:
        """Measure ADC voltage. Return raw 2-byte data
        Output value: voltage encoded in 2 bytes"""
        if self._mode == self._BBMODE:
            self.send_bytes([self.__VPROBE])
            ans = self.wait_recieve(self.TIME_READ)
            if ans is not None and len(ans) == 2:
                self.logger.info("Voltage measured")
                return ans
            else:
                self.logger.warning("Voltage measure unexpected response")
            return None
        else:
            self.logger.warning("Function is avaliable only in bitbang mode")
            return None

    def voltage(self) -> Optional[float]:
        """Measure ADC voltage
        Output value: voltage"""
        ans = self.voltage_raw()
        if ans is not None:
            return int.from_bytes(ans, byteorder="big") / 1024 * 3.3 * 2
        else:
            return None

    def frequency_raw(self) -> Optional[bytes]:
        """Measure AUX frequency. Return raw 4-byte data
        Output value: frequency encoded in 4 bytes"""
        if self._mode == self._BBMODE:
            self.send_bytes([self.__FPROBE])
            ans = self.wait_recieve(self.TIME_READ)
            if ans is not None and len(ans) == 4:
                self.logger.info("Frequency measured")
                return ans
            else:
                self.logger.warning("Frequency measure unexpected response")
            return None
        else:
            self.logger.warning("Function is avaliable only in bitbang mode")
            return None

    def frequency(self) -> Optional[int]:
        """Measure AUX frequency
        Output value: frequency"""
        ans = self.voltage_raw()
        if ans is not None:
            return int.from_bytes(ans, byteorder="big")
        else:
            return None

    def ddr(self, mask: int) -> Optional[int]:
        """Set data direction for pins AUX|MOSI|CLK|MISO|CS
        mask: bitmask of selected pins
        Output value: state of pins in form
        POWER|PULLUP|AUX|MOSI|CLK|MISO|CS"""
        if self._mode == self._BBMODE:
            self.send_bytes([self.__DDR | mask])
            ans = self.wait_recieve(self.TIME_READ)
            if ans is not None and len(ans) == 1:
                self.logger.info("Direction set")
                return int.from_bytes(ans, byteorder="big")
            else:
                self.logger.warning("Cannot set direction")
            return None
        else:
            self.logger.warning("Function is avaliable only in bitbang mode")
            return None

    def level(self, mask: int) -> Optional[int]:
        """Set level for pins POWER|PULLUP|AUX|MOSI|CLK|MISO|CS
        mask: bitmask of selected pins
        Output value: state of pins in form
        POWER|PULLUP|AUX|MOSI|CLK|MISO|CS"""
        if self._mode == self._BBMODE:
            self.send_bytes([self.__LEVEL | mask])
            ans = self.wait_recieve(self.TIME_READ)
            if ans is not None and len(ans) == 1:
                self.logger.info("Level set")
                return int.from_bytes(ans, byteorder="big") ^ self.__LEVEL
            else:
                self.logger.warning("Cannot set level")
            return None
        else:
            self.logger.warning("Function is avaliable only in bitbang mode")
            return None

    """Common in-mode function prototypes. Not avaliable in bitbang"""

    def _version_proto(self) -> Optional[str]:
        """Get mode type and version.
        Mode funciton. Not alaliable in bitbang mode
        Output value: mode """
        self.send_bytes([self.__GET_MODE])
        ans = self.wait_recieve(self.TIME_READ)
        anss: Optional[str]
        if ans is None:
            anss = None
        else:
            anss = ans.decode()
        return anss

    def _speed_proto(self, speed: Optional[int] = None) -> Optional[bytes]:
        """Get and set mode speed.
        Mode funciton. Not alaliable in bitbang mode
        speed: speed of the communication
        Output value: current speed. None return in case of errors"""
        if speed is None:
            self.send_bytes([self.__GET_SPEED])
        else:
            self.send_bytes([self.__SET_SPEED | speed])
        ans = self.wait_recieve(self.TIME_READ)
        return ans

    def _start_bit_proto(self) -> bool:
        """I2C start bit.
        Mode funciton. Not alaliable in bitbang mode
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__START_BIT], [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _stop_bit_proto(self) -> bool:
        """I2C stop bit.
        Mode funciton. Not alaliable in bitbang mode
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__STOP_BIT], [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _cs_low_proto(self) -> bool:
        """CS set low.
        Mode funciton. Not alaliable in bitbang mode
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__CS_LOW], [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _cs_high_proto(self) -> bool:
        """CS set high.
        Mode funciton. Not alaliable in bitbang mode
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__CS_HIGH], [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _clock_tick_proto(self) -> bool:
        """Clock tick.
        Mode funciton. Not alaliable in bitbang mode
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__CLOCK_TICK], [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _clock_low_proto(self) -> bool:
        """Clock set low.
        Mode funciton. Not alaliable in bitbang mode
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__CLOCK_LOW], [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _clock_high_proto(self) -> bool:
        """Clock set high.
        Mode funciton. Not alaliable in bitbang mode
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__CLOCK_HIGH], [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _data_low_proto(self) -> bool:
        """Clock data low.
        Mode funciton. Not alaliable in bitbang mode
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__DATA_LOW], [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _data_high_proto(self) -> bool:
        """Clock data high.
        Mode funciton. Not alaliable in bitbang mode
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__DATA_HIGH], [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _per_config_proto(self, mask: int) -> bool:
        """Peripherals configure.
        Mode funciton. Not alaliable in bitbang mode
        mask: int value of the peripherals settings
        w=power, x=pullups, y=AUX, z=CS
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__PER_CONFIG | mask],
                                [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _io_config_proto(self, mask: int) -> bool:
        """io configure.
        Mode funciton. Not alaliable in bitbang mode
        mask: int value of the peripherals settings
        w=output type, x=3wire, y=lsb, z=n/a
        Output value: true for success, false for failure"""
        ans = self.send_confirm([self.__IO_CONFIG | mask],
                                [self._MODE_CONFIRM])
        if ans is None:
            return False
        else:
            return ans[0]

    def _read_byte_proto(self) -> Optional[bytes]:
        """read byte.
        Mode funciton. Not alaliable in bitbang mode
        Output value: bytes read. None for failure"""
        self.send_bytes([self.__READ_BYTE])
        ans = self.wait_recieve(self.TIME_READ)
        if ans is None:
            return None
        else:
            return ans

    def _read_bit_proto(self) -> Optional[bytes]:
        """read bit.
        Mode funciton. Not alaliable in bitbang mode
        Output value: bytes read. None for failure"""
        self.send_bytes([self.__READ_BIT])
        ans = self.wait_recieve(self.TIME_READ)
        if ans is None:
            return None
        else:
            return ans

    def _peek_pin_proto(self) -> Optional[bytes]:
        """peek pin.
        Mode funciton. Not alaliable in bitbang mode
        Output value: bytes read. None for failure"""
        self.send_bytes([self.__PEEK_PIN])
        ans = self.wait_recieve(self.TIME_READ)
        if ans is None:
            return None
        else:
            return ans

    def _bulk_bytes_proto(self, data: List[int]) -> List[Optional[bytes]]:
        """bulk write bytes.
        Mode funciton. Not alaliable in bitbang mode
        data: list of data to write
        Output value: bytes read. None for failure"""
        N = 16
        ans = []
        while len(data) != 0:
            n = N if len(data) >= N else len(data)
            send_data = [self.__BULK_BYTES | (n - 1)]
            send_data = send_data + data[:n]
            self.send_bytes(send_data)
            data = data[n:]
            if self.recieve_bytes(1) == self._MODE_CONFIRM:
                ans.append(self.wait_recieve(self.TIME_READ))
            else:
                ans.append(None)
        return ans

    def _bulk_bits_proto(self, data: List[int]) -> List[Optional[bytes]]:
        """bulk write bits.
        Mode funciton. Not alaliable in bitbang mode
        data: list of data to write
        Output value: bytes read. None for failure"""
        ans = []
        N = 8
        while len(data) != 0:
            n = N if len(data) >= N else len(data)
            send_data = [self.__BULK_BITS | (n - 1)]
            send_data = send_data + data[:n]
            self.send_bytes(send_data)
            data = data[n:]
            if self.recieve_bytes(1) == self._MODE_CONFIRM:
                ans.append(self.wait_recieve(self.TIME_READ))
            else:
                ans.append(None)
        return ans

    def _bulk_clock_proto(self, number: int) -> Optional[List[bool]]:
        """bulk tick clock.
        Mode funciton. Not alaliable in bitbang mode
        number: number of clock ticks
        Output value: bytes read. None for failure"""
        N = 16
        msg: List[int] = []
        while number != 0:
            n = N if number >= N else number
            msg = msg + [self.__BULK_CLOCK | (n - 1)]
        return self.send_confirm(msg,
                                 [self._MODE_CONFIRM
                                  for _ in range(0, len(msg))])

    def _bulk_read_proto(self, number: int) -> List[Optional[bytes]]:
        """bulk read.
        Mode funciton. Not alaliable in bitbang mode
        number: number of clock ticks
        Output value: bytes read. None for failure"""
        ans: List[Optional[bytes]] = []
        N = 16
        while number != 0:
            n = N if number >= N else number
            send_data = [self.__BULK_CLOCK | (n - 1)]
            self.send_bytes(send_data)
            if self.recieve_bytes(1) == self._MODE_CONFIRM:
                ans.append(self.wait_recieve(self.TIME_READ))
            else:
                ans.append(None)
        return ans

    # TODO find out how smps works
    def smps_start(self, value: Optional[int] = None) -> Optional[int]:
        """set or get smps.
        value: int value to set. None to get
        Output value: bytes read. None for failure"""
        MIN = 0b_0010_0000_0000
        MAX = 0b_1111_1111_1111
        if value is None:
            self.send_bytes([self.__SMPS_GET])
        else:
            if not (MIN < value and value < MAX):
                self.logger.error("value is out of range [%d, %d]", MIN, MAX)
                return None
            else:
                self.send_bytes([self.__SMPS_START | (0xf & value >> 8),
                                 0xff & value])
        ans = self.wait_recieve(self.TIME_READ)
        if ans is None:
            return None
        else:
            print(ans)
            return int.from_bytes(ans, byteorder="big")

    def smps_stop(self) -> Optional[int]:
        """stop smps.
        Mode funciton. Not alaliable in bitbang mode
        Output value: bytes read. None for failure"""
        self.send_bytes([self.__PEEK_PIN])
        ans = self.wait_recieve(self.TIME_READ)
        if ans is None:
            return None
        else:
            return int.from_bytes(ans, byteorder="big")
